<?php
/**
 * Created By: Oli Griffiths
 * Date: 24/05/2012
 * Time: 14:01
 */
defined('KOOWA') or die('Protected resource');

class KMessage extends KObject
{

	public static function clear($identifier = null, $type = null, $key = null)
	{
		KRequest::set('session.messages'.($identifier ? '.'.$identifier : '').($type ? '.'.$type : '').($key ? '.'.$key : ''), null);
		KRequest::set('session.messages'.($identifier ? '.'.$identifier : '').($type ? '.'.$type : '').($key ? '.'.$key : ''), array());
	}


	public static function setMessage($message, $type = 'message', $identifier = null, $key = null, $append = false)
	{
		if(!$identifier) $identifier = '_global';
		if(!$key) $key = '_global';

        $msg = array('message' => $message, 'type' => $type);

		//If the key is the global key, this is an array so add to it
		if($key == '_global'){
			$messages = self::getMessages($identifier, $type, $key);
			$messages[] = $msg;
            $msg = $messages;
		}

        if($append){
            $messages = self::getMessages($identifier, $type, $key);

            if(!$messages) $messages = array();
            else if(isset($messages['type']) && isset($messages['message']) && count($messages) == 2){
                KMessage::clear($identifier, $type, $key);
                $messages = array($messages);
            }

            $messages[] = $msg;
            $msg = $messages;
        }

		//Set message
		KRequest::set('session.messages'.($identifier ? '.'.$identifier : '').($type ? '.'.$type : '').($key ? '.'.$key : ''), $msg);

		return $message;
	}


    public static function appendMessage($message, $type = 'message', $identifier = null, $key = null)
    {
        self::setMessage($message, $type, $identifier, $key, true);
    }


	public static function getMessage($identifier, $type, $key)
	{
		$message = self::getMessages($identifier, $type, $key);
        return current($message);
	}


	public static function getMessages($identifier = null, $type = null, $key = null)
	{
        return (array) KRequest::get('session.messages'.($identifier ? '.'.$identifier : '').($type ? '.'.$type : '').($key ? '.'.$key : ''), 'raw');
	}


	public static function getAllMessages($identifier = null){
		$messages = self::getMessages($identifier);
		return self::_flatten($messages);
	}


	public static function getMessagesByType($type, $identifier = null, $key = null)
	{
		$messages = self::getMessages($identifier);
		return self::_filterByType($messages->toArray(), $type);
	}


	protected static function _filterByType($messages, $type, $parent_type = ''){

		$return = array();
		foreach($messages AS $key => $message){
			if(is_array($message) && $key != '_global'){
				$return = array_merge($return, self::_filterByType($message, $type, $key));
			}elseif($key == '_global'){
				$return = array_merge($return, $message);
			}else{
				if($parent_type == $type) $return[$key] = $message;
			}
		}

		return $return;
	}


	protected static function _flatten($messages)
	{
		$return = array();

        if(!is_array($messages)) return $return;

		foreach($messages AS $key => $message){

            if(is_array($message) && isset($message['type']) && isset($message['message']) && count($message) == 2){
                $return[] = $message;

            }else{
                $return = array_merge($return, self::_flatten($message));
			}
		}

		return $return;
	}
}
