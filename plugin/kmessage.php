<?php
/**
 * KMessage Plugin
 *
 * A simple plugin to queue general system error messaged raised by KMessage
 * into the Joomla Mainframe message queue.
 *
 * @author      Kyle Waters <development@organic-development.com>
 * @category    KMessage
 */
defined('_JEXEC') or die('Restricted access');

jimport( 'joomla.plugin.plugin' );

class plgSystemKmessage extends JPlugin
{
	public function onAfterInitialise()
	{
		require_once dirname(__FILE__).'/../message.php';
	}

	public function onAfterDispatch ()
	{
		$mainframe = JFactory::getApplication();
		$global_messages = KMessage::getAllMessages();
		KMessage::clear();
		foreach($global_messages as $message)
        {
            if(is_array($message['message'])) $message['message'] = implode(', ',$message['message']);
			$mainframe->enqueueMessage($message['message'], $message['type']);
		}
	}
}